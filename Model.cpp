#include "Model.h"
#include <QThread>

Model::Model() : temp(0.0),hum(0.0)
{
     bck = new Backend();

    QThread* thread = new QThread;

    bck->moveToThread(thread);
    // connect(bck, SIGNAL (error(QString)),this, SLOT (errorString(QString)));

    QThread::connect(thread, &QThread::started, bck, &Backend::_run);
    QThread::connect( bck, &Backend::_finished, thread, &QThread::quit);
    QThread::connect(bck, SIGNAL (_finished()), bck, SLOT (deleteLater()));
    QThread::connect(thread, SIGNAL (finished()), thread, SLOT (deleteLater()));

    QThread::connect(bck, &Backend::temperatureChanged, this, &Model::set_Temp);
    QThread::connect(bck, &Backend::humidityChanged, this, &Model::set_humidity);
    thread->start();

    mqtt_client = new MqttClient();
    mqtt_client->mqttConnect();

}

Model::~Model()
{
    delete  bck;
    delete mqtt_client;

}
void Model::set_Temp(float f)
{
    Model::temp = f;
    emit dataChanged();
}


float Model::temperature() const
{
    return Model::temp;
}

void Model::set_humidity(float f)
{
    Model::hum = f;
    emit dataChanged();
}


float Model::humidity() const
{
    return Model::hum;
}


QString Model::getDevicesName()
{
   QString devices;
   return  devices;
}
int  Model::getDevicesCmd()
{
    int cmd ;
    return  cmd ;
}

void Model::writeDevicesName(QString)
{

}

void Model::writeDevicesCmd(int)
{

}
