#ifndef Model_H
#define Model_H

#include <QMqttClient>
#include <QString>

#include "Backend.h"
#include "mqttclient.h"

class Model : public QObject
{
    Q_OBJECT
    Q_PROPERTY(float  temperature  READ temperature WRITE set_Temp  NOTIFY dataChanged)
    Q_PROPERTY(float  humidity  READ humidity WRITE set_humidity  NOTIFY dataChanged)

   Q_PROPERTY(QString devicesName READ getDevicesName WRITE writeDevicesName NOTIFY devicesNameChanged)
   Q_PROPERTY(int devicesCmd READ getDevicesCmd WRITE  writeDevicesCmd NOTIFY devicesCmdChanged)

    public:
           Model();
          ~Model();

    float  temperature()const;
    float  humidity()const;
    QString getDevicesName();
     int    getDevicesCmd();

public slots:

     void writeDevicesName(QString);
     void writeDevicesCmd(int);

     void set_Temp(float f);
     void set_humidity(float f);

signals:
     void devicesNameChanged();
     void devicesCmdChanged();
     void error(QString err);

     void dataChanged();

private:
     MqttClient* mqtt_client;
      Backend* bck ;
     float temp ,hum;
};

#endif // Model_H
