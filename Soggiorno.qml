import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Window 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.12
import Qt.labs.settings 1.0
import "./qml"

import "."

Page {
    width: 1200
    height:  800

    font.family: "Arial"
    font.pixelSize: 30
    title: qsTr("Soggiorno")

    Rectangle{
        id: rectangle
        color: "#a1c4fd"
        anchors.rightMargin: -93
        anchors.fill: parent
        width: Constants.width
        height:  Constants.heigh
        /*
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#e3fdf5"
            }

            GradientStop {
                position: 1
                color: "#ffe6fa"
            }
        }

      anchors.fill: parent

      Image {
          id: bg
          anchors.fill: parent
          fillMode: Image.PreserveAspectFit
          source: "icons/sg.png"
      }

      Label {
          color: "#1f78cf"
          text: qsTr("Benvenuto nel tuo Soggiorno")
          font.family: "Times New Roman"
          anchors.verticalCenterOffset: -190
          anchors.horizontalCenterOffset: -50
          font.italic: true
          font.pixelSize: 33
          horizontalAlignment: Text.AlignHCenter
          anchors.centerIn: parent
      }
    */
      StackLayout {
          id:svipeView
          anchors.bottomMargin: 49
          anchors.leftMargin: 244
          anchors.bottom: tabBar.bottom
          anchors.right: parent.right
          anchors.left: sideBar.left
          anchors.top: parent.top
          visible: true
              currentIndex: tabBar.currentIndex

              Light{
                  id:light
                  width: rectangle.width - sideBar.width
                anchors.fill: parent
              }

              Heating{

              }

              Security{

              }

          }
      TabBar {
          id:tabBar
          x: 0
          y: 831
          width: 1013
          height: 49
          visible: true
          currentIndex: svipeView.currentIndex

          TabButton{
              text:qsTr("Lights")
              checkable: false
              font.pixelSize: 22
          }

          TabButton{
              text:qsTr("Heating")
              autoExclusive: true
              font.pixelSize: 22
          }

          TabButton{
              text:qsTr("Security")
              wheelEnabled: false
              font.pixelSize: 22
          }

          TabButton {
              x: 795
              width: 185
              height: 40
              text: qsTr("ViewCtr")
              anchors.top: parent.top
              wheelEnabled: false
              font.pixelSize: 22
          }

          Grid {
          }
      }

      SideBar {
          id: sideBar
          width: 245
          anchors.bottom: tabBar.top
          anchors.top: parent.top
          visible: true
      }
    }
}




/*##^##
Designer {
    D{i:6;anchors_height:49;anchors_width:920;anchors_x:0;anchors_y:831}D{i:12;anchors_width:245}
}
##^##*/
