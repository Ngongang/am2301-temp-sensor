import QtQuick 2.12
import QtQuick.Controls 2.5
import "."

Page {
    width: 720
    height: 480

    title: qsTr("Bagno")

    Rectangle {
        anchors.fill: parent
        color: "#31425f"

        Label {
            color: "#1f78cf"
            text: qsTr("Benvenuto nel tuo Bagno")
            font.family: "Times New Roman"
            anchors.verticalCenterOffset: -142
            anchors.horizontalCenterOffset: 1
            font.italic: true
            font.pixelSize: 33
            horizontalAlignment: Text.AlignHCenter
            anchors.centerIn: parent
        }
    }
}
