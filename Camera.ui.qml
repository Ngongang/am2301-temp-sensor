import QtQuick 2.12
import QtQuick.Controls 2.5
import "."

Page {
    width: Constants.width
    height: Constants.height

    title: qsTr("Camera")

    Rectangle {
        anchors.fill: parent
        color: Constants.backgroundColor
        Label {
            text: qsTr("Benvenuto in Camera")
            anchors.centerIn: parent
        }
        Image {
            id: img
            sourceSize.height: 480
            sourceSize.width: 720
            fillMode: Image.PreserveAspectCrop
            source: "icons/smartR.png"
        }
    }
}
