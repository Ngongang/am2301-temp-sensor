import QtQuick 2.12
import QtQuick.Controls 2.5

Page {
    width: 880
    height: 980
    font.family: "Arial"
    font.pixelSize: 30
    title: qsTr("Home")

    Rectangle {
        id: root
        color: "#1a1e25"
        anchors.fill: parent
        state: "Standard"

        Label {
            width: 508
            height: 33
            color: "#a9c8e7"
            text: qsTr("Welcome to Your Smart Home Device")
            anchors.verticalCenterOffset: -190
            anchors.horizontalCenterOffset: 0
            anchors.centerIn: parent
        }

        Label {
            id: label
            x: 34
            y: 133
            color: "#a9c8e7"
            text: qsTr(" Room Temperaure")
        }

        Label {
            id: label1
            x: 34
            y: 204
            color: "#a9c8e7"
            text: qsTr(" Room presure")
        }

        Label {
            id: label2
            x: 430
            y: 133
            color: "#dfe9f6"
            text: qsTr("Label")
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            id: label3
            x: 430
            y: 204
            color: "#dfe9f6"
            text: qsTr("Label")
            horizontalAlignment: Text.AlignHCenter
        }
    }
}
