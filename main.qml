import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Window 2.12
import QtMultimedia 5.0

import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.12
import Qt.labs.settings 1.0

import "./"

ApplicationWindow {
    id: window
    visible: true
    width: 1200
    height:  800
    color: "#1a1e25"
    title: qsTr("StackApp")
    visibility: "FullScreen"

    header: ToolBar {
        contentHeight: toolButton.implicitHeight

        ToolButton {
            id: toolButton
            text: stackView.depth > 1 ? "\u25C0" : "\u2630"
            font.capitalization: Font.Capitalize
            font.weight: Font.ExtraBold
            font.bold: true
            font.pixelSize: 30
            onClicked: {
                if (stackView.depth > 1) {
                    stackView.pop()
                } else {
                    drawer.open()
                }
            }
        }

        Label {
            color: "#0864c0"
            text: stackView.currentItem.title
            font.bold: true
            font.capitalization: Font.Capitalize
            font.family: "Times New Roman"
            font.pixelSize: 30
            anchors.centerIn: parent
        }
    }

    Drawer {
        id: drawer
        width: window.width * 0.66
        height: window.height
        Rectangle {
                    anchors.fill: parent
                    color: "#31425f"
                    Rectangle{
                        id:space
                        width: 0
                        height: 20
                    }

                    Column {
                        x: 10
                        anchors.top: space.bottom
                        anchors.right: parent.right
                        anchors.bottom: parent.bottom
                        anchors.left: parent.left
                        spacing: 5
                        ItemDelegate {
                            Text {
                                id: alarms
                                x: 15
                                text: qsTr("Alarms")
                                verticalAlignment: Text.AlignVCenter
                                style: Text.Raised
                                textFormat: Text.RichText
                                fontSizeMode: Text.Fit
                                font.weight: Font.Thin
                                font.family: "Times New Roman"
                                font.italic: true
                                font.pixelSize: 30
                                horizontalAlignment: Text.AlignLeft
                                color: "#28db88"
                                width: 20
                                height: 20
                            }
                            width: parent.width
                            onClicked: {
                                stackView.push("alarms/Alarms.qml")
                                drawer.close()
                            }
                        }
                ItemDelegate {
                    Text {
                        id: soggiorno
                        x: 15
                        text: qsTr("Soggiorno")
                        verticalAlignment: Text.AlignVCenter
                        style: Text.Raised
                        textFormat: Text.RichText
                        fontSizeMode: Text.Fit
                        font.weight: Font.Thin
                        font.family: "Times New Roman"
                        font.italic: true
                        font.pixelSize: 30
                        horizontalAlignment: Text.AlignLeft
                        color: "#28db88"
                        width: 20
                        height: 20
                    }
                    width: parent.width
                    onClicked: {
                        stackView.push("Soggiorno.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    Text {
                        id: camera
                        x: 15
                        text: qsTr("Camera")
                        style: Text.Raised
                        textFormat: Text.RichText
                        fontSizeMode: Text.Fit
                        font.weight: Font.Thin
                        font.family: "Times New Roman"
                        font.italic: true
                        font.pixelSize: 30
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft
                        color: "#28db88"
                        width: 20
                        height: 20
                    }
                    width: parent.width
                    onClicked: {
                        stackView.push("CameraView.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    Text {
                        id: bagno
                        text: qsTr("Bagno")
                        style: Text.Raised
                        x: 15
                        textFormat: Text.RichText
                        fontSizeMode: Text.Fit
                        font.weight: Font.Thin
                        font.family: "Times New Roman"
                        font.italic: true
                        font.pixelSize: 30
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft
                        color: "#28db88"
                        width: 20
                        height: 20
                    }
                    width: parent.width
                    onClicked: {
                        stackView.push("Bagno.ui.qml")
                        drawer.close()
                    }
                }

                ItemDelegate {
                    Text {
                        id: videocam
                        text: qsTr("VideoCamera")
                        style: Text.Raised
                        x: 15
                        textFormat: Text.RichText
                        fontSizeMode: Text.Fit
                        font.weight: Font.Thin
                        font.family: "Times New Roman"
                        font.italic: true
                        font.pixelSize: 30
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft
                        color: "#28db88"
                        width: 20
                        height: 20
                    }
                    width: parent.width
                    onClicked: {
                        stackView.push("camera/SceneBasic.qml")
                        drawer.close()
                    }
                }

                ItemDelegate {
                    Text {
                        id: quit
                        text: qsTr("Exit")
                        style: Text.Raised
                        x: 15
                        textFormat: Text.RichText
                        fontSizeMode: Text.Fit
                        font.weight: Font.Thin
                        font.family: "Times New Roman"
                        font.italic: true
                        font.pixelSize: 30
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft
                        color: "#28db88"
                        width: 20
                        height: 20
                    }
                    width: parent.width
                    onClicked: Qt.quit();

                }
            }
        }
    }

    StackView {
        id: stackView
        font.pixelSize: 30
        font.family: "Arial"
        initialItem: "Home.qml"
        anchors.fill: parent
        background: window.background
    }
}
