#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QSettings>
#include <QIcon>

#include <QQmlContext>
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlEngine>
#include <QtQuick/QQuickItem>
#include <QLoggingCategory>
#include <QObject>
#include <QThread>
#include "Model.h"

int main(int argc, char *argv[])
{
   // QLoggingCategory::setFilterRules("wapp.*.debug=true");
    QGuiApplication::setApplicationName("SmartApp");
    QGuiApplication::setOrganizationName("QtProject");
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    // qmlRegisterType<Backend>("Termostat", 1, 0, "Backend");


//    Backend* bck = new Backend();

//    QThread* thread = new QThread;

//    bck->moveToThread(thread);
//    // connect(bck, SIGNAL (error(QString)),this, SLOT (errorString(QString)));

//    QThread::connect(thread, &QThread::started, bck, &Backend::_run);
//    QThread::connect( bck, &Backend::_finished, thread, &QThread::quit);
//    QThread::connect(bck, SIGNAL (_finished()), bck, SLOT (deleteLater()));
//    QThread::connect(thread, SIGNAL (finished()), thread, SLOT (deleteLater()));

//    thread->start();

    Model model;

    QIcon::setThemeName("Smart Home");
    qDebug() << "main initialized";
    QQmlApplicationEngine engine;

    engine.rootContext()->setContextProperty("termostat", &model);
    engine.addImportPath("qrc:/");

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                    &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
   }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
