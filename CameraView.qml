import QtQuick 2.12
import QtQuick.Controls 2.5
import QtMultimedia 5.12

Item {
    id:view

    Camera {
        id: camera
        imageProcessing.whiteBalanceMode: CameraImageProcessing.WhiteBalanceFlash

        exposure {
            exposureCompensation: -1.0
            exposureMode: Camera.ExposurePortrait
        }

        flash.mode: Camera.FlashRedEyeReduction

        imageCapture {
            onImageCaptured: {
                // Show the preview in an Image
                photoPreview.source = preview
            }
        }
    }

    VideoOutput {
        anchors.fill: parent
        source: camera
        focus: visible

        MouseArea {
            anchors.fill: parent
            onClicked: camera.imageCapture.capture()
        }
    }

    Image {
        id: photoPreview
    }

    ListView {
        anchors.fill: parent

        model: QtMultimedia.availableCameras
        delegate: Text {
            text: modelData.displayName
            color: "white"
            MouseArea {
                anchors.fill: parent
                onClicked: camera.deviceId = modelData.deviceId
            }
        }
    }
}
