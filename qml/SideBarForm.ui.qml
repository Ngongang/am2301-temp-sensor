import QtQuick 2.4
import QtQuick.Controls 2.0

PageBackground {
    width: 245
    height: 880
    color: "#915151"
    property alias customLabel1Text: customLabel1.text
    property alias comboBox: comboBox
    property alias button: button
    title: "Smart Home"

    Label {
        id: label
        x: 2
        y: 117
        width: 102
        height: 25
        color: "#4dea70"
        text: Qt.formatDateTime(new Date(), "dddd")
        font.pixelSize: 22
    }

    CustomLabel {
        id: customLabel1
        x: 110
        y: 117
        width: 127
        height: 25
        text: Qt.formatDateTime(new Date(), "dd/MM/yyyy")
        font.pixelSize: 22
    }

    Label {
        id: label1
        x: 0
        y: 163
        color: "#4dea70"
        text: qsTr("Temperature")
        font.family: "Arial"
        font.pixelSize: 22
    }

    CustomLabel {
        id: customLabel2
        x: 109
        y: 194
        width: 112
        height: 25
        text: "22 degree"
        font.pixelSize: 22
    }

    Label {
        id: label2
        x: 12
        y: 234
        color: "#212722"
        text: qsTr("Power Consumption")
        font.pixelSize: 22
    }

    ProgressBar {
        id: progressBar
        x: 21
        y: 265
        width: 200
        height: 13
        value: 0.5
    }

    Switch {
        id: element
        x: 23
        y: 284
        text: qsTr("Vendilation")
        autoExclusive: false
        font.pixelSize: 22
    }

    Slider {
        id: slider
        x: 22
        y: 338
        value: 0.5
    }

    Switch {
        id: element1
        x: 21
        y: 392
        text: qsTr("Alarm Active")
        font.family: "Arial"
        font.pixelSize: 22
    }

    RadioButton {
        id: radioButton
        x: 34
        y: 440
        text: qsTr("Active Alert")
        font.pixelSize: 22
        font.family: "Arial"
    }

    Label {
        id: label3
        x: 74
        y: 504
        text: qsTr("Mode")
        font.pixelSize: 22
    }

    ComboBox {
        id: comboBox
        x: 21
        y: 535
        width: 201
        height: 51
    }

    Button {
        id: button
        x: 11
        y: 617
        width: 200
        height: 37
        text: qsTr("Configure")
    }

    CustomLabel {
        id: customLabel
        x: 54
        y: 59
        width: 115
        height: 25
        font.pixelSize: 22
    }
}
