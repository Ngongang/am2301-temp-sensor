#ifndef BACKEND_H
#define BACKEND_H

#include <wiringPi.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <syslog.h>
#include <sys/sysinfo.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <QObject>
#include <QString>
#include <qqml.h>
#include <QtQml>

class Backend : public QObject
{
    Q_OBJECT

    public:
        explicit Backend(QObject *parent = nullptr);
          ~Backend();

        typedef struct __sensor_data {
            float rh;
            float t;
        } sensor_data;

    void set_Temp(float f);
    float  temperature()const;
    void set_humidity(float f);
    float  humidity()const;

    public slots:
        void _run();
        // void errorString(QString err);
        float get_temperature(void);
        float get_presure(void);

    signals:
        void _finished();
        void _error(QString err);
        void humidityChanged(float f);
        void temperatureChanged(float f);

    private:
       void do_init(void);
       void _quit(void);
       int wait_change(int mode, unsigned int tmo);
       int read_am2301(sensor_data *s, int mode);

      // const int _pin_am2301 = 12; // data pin which is 5 pin count from end right
       //  pin 4 is ground count from end right
       sensor_data private_data;
       int sig = 0;
};

#endif // BACKEND_H
