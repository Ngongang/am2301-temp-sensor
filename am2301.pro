TARGET = am2301
CONFIG += qt c++11
QT += quick mqtt network
QT += concurrent

LIBS += -L$$PWD -lwiringPi3 -lcrypt -lrt

INCLUDEPATH += /opt/builds/qt5/sysroot/usr/include
INCLUDEPATH +=  $$PWD/wiringPi
DEBUG = 0
 #Default rules for deployment.
#target.path = /usr/local/bin
#!isEmpty(target.path): INSTALLS += target


target.path = /home/pi
!isEmpty(target.path): INSTALLS += target

SOURCES += \
    Backend.cpp \
    Model.cpp \
    main.cpp \
    mqttclient.cpp \
    tempRh.c

DISTFILES += \
    CameraView.qml \
    HomeForm.ui.qml \
    Bagno.ui.qml \
    Camera.ui.qml \
    alarms/AlarmDelegate.qml \
    alarms/AlarmDialog.qml \
    alarms/AlarmModel.qml \
    alarms/TumblerDelegate.qml \
    alarms/Alarms.qml \
    alarms/qtquickcontrols2.conf \
    am2301.env \
    am2301.service \
    camera/Button.qml \
    camera/CameraBasic.qml \
    camera/CameraItem.qml \
    camera/Content.qml \
    camera/Scene.qml \
    camera/SceneBasic.qml \
    icons/pressure.png \
    icons/rh.png \
    icons/sg.png \
    icons/smartR.png \
    icons/temp.png \
    libwiringPi3.a \
    main.qml \
    qtquickcontrols2.conf

RESOURCES += \
    qml.qrc \
    HomeForm.ui.qml \
    Bagno.ui.qml \
    Camera.ui.qml \
    main.qml \
   alarms/Alarms.qml \
    icons/pressure.png \
    icons/temp.png \

HEADERS += \
    Backend.h \
    Model.h \
    mqttclient.h
