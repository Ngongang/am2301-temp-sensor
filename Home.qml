import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.12
import Qt.labs.settings 1.0
import "./qml"

Page {
    width: root.width
    height: root.height
    font.family: "Arial"
    font.pixelSize: 30
    title: qsTr("Home")

    Material.theme: Material.Dark
    Material.accent: Material.DeepOrange

    Rectangle {
        id: root
        color: "#033452"
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.fill: parent
        state: "Standard"

        Label {
            width: 508
            height: 33
            color: "#a9c8e7"
            text: qsTr("Welcome to Your Smart Home Device")
            anchors.verticalCenterOffset: -190
            anchors.horizontalCenterOffset: 0
            anchors.centerIn: parent
        }

        Image {
            id: temp
            x: 147
            y: 93
            width: 104
            height: 113
            fillMode: Image.PreserveAspectFit
            source: "icons/temp.png"
        }
        Image {
            id: pre
            x: 106
            y: 237
            width: 158
            height: 126
            clip: false
            visible: true
            fillMode: Image.PreserveAspectFit
            source: "icons/rh.png"
        }
        Label {
            id: label
            x: 114
            y: 212
            width: 192
            height: 22
            color: "#cee4d4"
            text: qsTr("Temperature Soggiorno")
            font.family: "Times New Roman"
            font.pixelSize: 20
        }

        Label {
            id: label1
            x: 114
            y: 359
            color: "#a9c8e7"
            text: qsTr("Umidità Soggiorno ")
            font.family: "Times New Roman"
            font.pixelSize: 20
        }

        Label {
            id: tempe
            x: 278
            y: 86
            color: "#dfe9f6"
            text: termostat.temperature
            font.pixelSize: 100
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            id: presure
            x: 270
            y: 244
            color: "#dfe9f6"
            text: termostat.humidity
            font.pixelSize: 100
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            id: label2
            y: 99
            color: "#dfe9f6"
            text: qsTr("°C")
            anchors.left: tempe.right
            font.italic: true
            font.family: "Times New Roman"
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 40
        }

        Label {
            id: label3
            y: 274
            width: 60
            height: 37
            color: "#dfe9f6"
            text: qsTr("%")
            anchors.left: presure.right
            font.pixelSize: 33
        }
    }

}

/*##^##
Designer {
    D{i:9;anchors_x:415}D{i:10;anchors_x:422}
}
##^##*/
