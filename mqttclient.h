#ifndef MQTTCLIENT_H
#define MQTTCLIENT_H

#include <QMqttClient>
#include <QString>

class MqttClient :  public QObject
{
     Q_OBJECT

    typedef struct
    {
        QString deviceName;
        QChar deviceCmd;

    }devInfo_t;

public:

    explicit MqttClient(QObject *parent = nullptr);
   ~MqttClient();

    quint16 mqtt_port = 1883;
    const QString mqtt_host = "localhost";

    void mqttConnect();
     void buttonPublish_clicked();

public slots:
     //std::vector<QString> devicesName();
    // std::vector<int> devicesCmd();

signals:
     void error(QString err);

private slots:
    void buttonConnect_clicked();
    void buttonQuit_clicked();
    void updateLogStateChange();

    void brokerDisconnected();

    void buttonSubscribe_clicked();

    void processReceivedMsg(const QByteArray &msg ,const QString &str);

private:
     QMqttClient *m_client;
     QMap<QString,QChar> devInfo;
};

#endif // MQTTCLIENT_H
