#include "Backend.h"
#include <signal.h>
static void quit_handler(int sig);

#define	LED	0
/* Connect DATA to GPIO18 (pin 12). In wiringPi pin number is 1 */
static const int _pin_am2301 = 26;

Backend::Backend(QObject *parent) : QObject(parent)
{
    Backend::private_data.t = 20.0;
    Backend::private_data.rh = 0.0;
}

Backend::~Backend(){
    Backend::_quit();
}

static void quit_handler(int sig)
{
    signal(sig, SIG_IGN);
    exit(0);
}

 void Backend::_quit(void)
{
   emit Backend::_finished();
   quit_handler(SIGTERM);
}
 void Backend::do_init(void)
{
    if (wiringPiSetup() == -1) {
        printf("wiringPi-Error\n");
        emit Backend::_error("wiringPi-Error\n");
        Backend::_quit();
    }

    signal (SIGTERM, quit_handler);
    signal (SIGHUP, quit_handler);

    // piHiPri(20);
}

 int Backend::wait_change(int mode, unsigned int tmo)
{
    int v1, v2, v3;
    unsigned int now = micros();

    do {
    /* Primitive low-pass filter */
    v1 = digitalRead(_pin_am2301);
    v2 = digitalRead(_pin_am2301);
    v3 = digitalRead(_pin_am2301);
    if (v1 == v2 && v2 == v3 && v3 == mode) {
        return (micros() - now);
    }
    } while ((micros() - now) < tmo);
    return -1;
}

int Backend::read_am2301(Backend::sensor_data *s, int mode)
{
    int i, j, k;
    int val;
    int v;
    unsigned char x;
    unsigned char vals[5];

    /* Leave it high for a while */
    pinMode(_pin_am2301, OUTPUT);
    digitalWrite(_pin_am2301, HIGH);
    delayMicroseconds(100);

    /* Set it low to give the start signal */
    digitalWrite(_pin_am2301, LOW);
    delayMicroseconds(1000);

    /* Now set the pin high to let the sensor start communicating */
    digitalWrite(_pin_am2301, HIGH);
    pinMode(_pin_am2301, INPUT);
    if (wait_change(HIGH, 100) == -1) {
    return -1;
    }

    /* Wait for ACK */
    if (wait_change(LOW, 100) == -1) {
    return -2;
    }

    if (wait_change(HIGH, 100) == -1) {
    return -3;
    }

    /* When restarting, it looks like this lookfor start bit is not needed */
    if (mode != 0) {
    /* Wait for the start bit */
    if (wait_change(LOW, 200) == -1) {
        return -4;
    }

    if (wait_change(HIGH, 200) == -1) {
        return -5;
    }
    }
    x = 0;
    k = 0;
    j = 7;

    for (i = 0; i < 40; i++) {
    val = wait_change(LOW, 500);
    if (val == -1) {
        return -6;
    }

    v = (val >= 50) ? 1 : 0;
    x = x | (v << j);

    if (--j == -1) {
        vals[k] = x;
        k++;
        j = 7;
        x = 0;
    }
    val = wait_change(HIGH, 500);
    if (val == -1) {
        return -7;
    }
    }
    pinMode(_pin_am2301, OUTPUT);
    digitalWrite(_pin_am2301, HIGH);

    /* Verify checksum */
    x = vals[0] + vals[1] + vals[2] + vals[3];
    if (x != vals[4]) {
    return -8;
    }

    s->rh = (float) (((uint16_t) vals[0] << 8) | (uint16_t) vals [1]);
    s->rh /= 10.0f;
    s->t = (float) (((uint16_t) vals[2] << 8) | (uint16_t) vals [3]);
    s->t /= 10.0f;

    if (s->rh > 100.0 || s->rh < 0.0 ||
    s->t > 80.0 || s->t < -40.0 )
    {
    return -9;
    }
    return 0;
}

void Backend::_run()
{
    int ret;
    sensor_data s;

    wiringPiSetup () ;

    do_init();
    /* Try 10 times, then bail out.
     */
    while (true) {
        ret = read_am2301(&s, 1);

        if (ret == 0){
            Backend::set_Temp(s.t);
            Backend::set_humidity(s.rh);
        }
        delay(2000);
    }
}

float Backend::get_temperature()
{
    return Backend::private_data.t;
}

float Backend::get_presure()
{
    return Backend::private_data.rh;
}

void Backend::set_Temp(float f)
{
    Backend::private_data.t = f;
    qDebug() << "temp is : " << Backend::private_data.t;
    emit temperatureChanged(f);
}


float Backend::temperature() const
{
    return Backend::private_data.t;
}

void Backend::set_humidity(float f)
{
    Backend::private_data.rh = f;
    qDebug() << "Humid is : " << Backend::private_data.rh;
    emit humidityChanged(f);
}


float Backend::humidity() const
{
    return Backend::private_data.rh;
}
