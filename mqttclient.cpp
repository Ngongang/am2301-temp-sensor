#include "mqttclient.h"
#include <QCoreApplication>
#include <QtCore/QDateTime>
#include <QtMqtt/QMqttClient>
#include <QtWidgets/QMessageBox>

MqttClient::MqttClient(QObject *parent) :   QObject(parent)
{
    MqttClient::m_client = new QMqttClient(this);
    MqttClient::m_client->setHostname(mqtt_host);
    MqttClient::m_client->setPort(mqtt_port);


    connect(MqttClient::m_client, &QMqttClient::stateChanged,this, &MqttClient::updateLogStateChange);
    connect(MqttClient::m_client, &QMqttClient::disconnected, this, &MqttClient::brokerDisconnected);

    connect(MqttClient::m_client, &QMqttClient::messageReceived, this, [this](const QByteArray &message, const QMqttTopicName &topic) {
        const QString content = QDateTime::currentDateTime().toString()
                    + QLatin1String(" Received Topic: ")
                    + topic.name()
                    + QLatin1String(" Message: ")
                    + message
                    + QLatin1Char('\n');

        MqttClient::processReceivedMsg(message, topic.name());

        qDebug() << content;
    });
}

MqttClient::~MqttClient()
{
    delete m_client;
}

void MqttClient::processReceivedMsg(const QByteArray &msg ,const QString &str)
{
    QStringList strTmp = str.split("/");
    MqttClient::devInfo.insert(strTmp.at(1),msg.toUInt());

      qDebug() << "command :" << strTmp.at(1) << "value" << msg.toUInt();
}
void MqttClient::mqttConnect()
{
   MqttClient::m_client->connectToHost();
}

/*
std::vector<QString>  MqttClient::devicesName()
{
   std::vector<QString> devices;
   return  devices;
}
std::vector<int>  MqttClient::devicesCmd()
{
    std::vector<int> cmd ;
    return  cmd ;
}
*/
void MqttClient::updateLogStateChange()
{
    const QString content = QDateTime::currentDateTime().toString()
                    + QLatin1String(": State Change")
                    + QString::number(MqttClient::m_client->state())
                    + QLatin1Char('\n');
    qDebug() << content ;

   if(MqttClient::m_client->state() == 2){

       MqttClient::buttonSubscribe_clicked();
   }
}

void MqttClient::brokerDisconnected()
{
    qDebug() << "broker is Disconnected";
}


void MqttClient::buttonPublish_clicked()
{
    if (m_client->publish(QLatin1String("feed/onOff"), "Hello world") == -1){
        qDebug() << "Could not publish message" ;
    }else {
        qDebug() << "published message" ;
    }
}

void MqttClient::buttonSubscribe_clicked()
{
    auto subscription = m_client->subscribe(QLatin1String("feed/+"));
    if (!subscription) {
       qDebug() << "Could not subscribe. Is there a valid connection?";
        return;
    }else {
        MqttClient::buttonPublish_clicked();
    }
}

void MqttClient::buttonQuit_clicked()
{
    QCoreApplication::quit();
}

void MqttClient::buttonConnect_clicked()
{
    if (m_client->state() == QMqttClient::Disconnected) {
        m_client->connectToHost();
    } else {
        m_client->disconnectFromHost();
    }
}
